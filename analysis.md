Анализ предметной области
=========================

## 1. Характеристика объекта автоматизации

### 1.1	Характеристика деятельности предприятия

Общество с ограниченной ответственностью «СТФ» было создано в 2015 году и располагается по адресу г. Москва, Пресненская набережная 6 строение 2. Организация является юридическим лицом, имеет самостоятельный баланс, расчетный счет в банке, печать установленного образца со своим наименованием, а также иные необходимые для осуществления своей деятельности печати, штампы и фирменные бланки.

Основными видами деятельности ООО «СТФ» являются:

- предоставление брокерских услуг на рынке форекс,
- предоставление ликвидности и программно-аппаратных решений для компаний-партнеров.

Общество имеет гражданские права и несет гражданские обязанности для осуществления любых видов деятельности, не запрещенных федеральными законами Российской Федерации.

В своей деятельности ООО «СТФ» руководствуется федеральными законами и нормативными правовыми актами Российской Федерации, а также уставом компании.

### 1.2	Организационно-управленческая структура предприятия 

Организационная структура управления предприятием построена по функциональному принципу. 
Рассмотрим часть структуры в рамках автоматизируемых процессов.

*Рисунок 1 — Схема организационной структуры ООО «СТФ»*
![Схема организационной структуры ООО «СТФ»](diagrams/organizational_structure.png)

### 1.3	Общая характеристика применяемых в системе технических и программных средств

В своей работе сотрудники отдела продаж используют ноутбуки для работы с CRM-системой, и телефонные аппараты для осуществления звонков клиентам посредством SIP телефонии.

Конфигурация оборудования рабочего места

| Наименование       | Характеристики                |
| ------------- |:------------------:|
| Процессор     | Intel Celeron 2.7 GHz x2/ integrated graphics  |
| Оперативная память     | 4Gb    |
| Жесткий диск  | SSD 128 ГБ         |
| Сеть  | WiFi 802.11 b/g/n, GbLAN         |
| Монитор  | 15,6-дюймовый HD-дисплей (1366 x 768)         |
| IP-Телефон  | Yealink SIP-T27G         |
*Таблица 1 — Конфигурация оборудования рабочего места*

Программное обеспечение рабочих мест:

+	Операционная система Windows 10 — пользовательская операционная система семейства Windows NT;
+	Браузер Google Chrome — web-браузер, разрабатываемый компанией Google на основе свободного браузера Chromium и движка Blink;
+	торговый терминал MT4 — клиентская часть торговой платформы, предназначена для проведения торговых операций и технического анализа в режиме реального времени;
+    офисный пакет MS Office 2007 Standard.

Каждый региональный отдел продаж имеет свой IP-АТС сервер для осуществления звонков.

| Наименование       | Характеристики|
| ------------- |:------------------:|
| Процессор     | Intel(R) Core(TM) i7-7700 CPU @ 3.60GHz x8  |
| Оперативная память     | 32Gb    |
| Жесткий диск  | SSD 512 ГБ         |
| Сеть  | 10 Gbit LAN         |
*Таблица 2 — Конфигурация сервера АТС*

На серверах установлено следующее ПО:

+	операционная система Linux CentOS
+	IP-ATC Asterisk 
+	СУБД MySQL, для хранения служебной информации АТС
+	FreePBX, для управления АТС.

CRM-система, общая для всех региональных подразделений. Выполняет функции фронтальной части предназначенной для обслуживания клиентов с централизованной обработкой информации, операционной части, обеспечивающей отчетность о совершенных операциях с контролем доступа к информации, хранилище данных, аналитическую подсистему для оперативного анализа и контроля работы.
Распределена на сервер баз данных и сервер приложений, где сервер приложений обслуживает множество пользователей системы, а сервер баз данных выполняет исключительно функции хранилища.

Конфигурация сервера приложений

| Наименование       | Характеристики|
| ------------- |:------------------:|
| Процессор     | Intel(R) Xeon(R) CPU D-1541 @ 2.10GHz x16  |
| Оперативная память     | 64Gb    |
| Жесткий диск  | SSD 512 ГБ x2         |
| Сеть  | 10 Gbit LAN         |
*Таблица 3 — Конфигурация сервера приложений*

ПО сервера приложений:

+	операционная система Linux Ubuntu
+	Nginx веб-сервер 
+	PHP-FPM менеджер процессов.

Конфигурация сервера баз данных

| Наименование       | Характеристики|
| ------------- |:------------------:|
| Процессор     | Intel(R) Xeon(R) CPU D-1541 @ 2.10GHz x16  |
| Оперативная память     | 32Gb    |
| Жесткий диск  | SSD 512 ГБ x2         |
| Сеть  | 10 Gbit LAN         |
*Таблица 4 — Конфигурация сервера баз данных*

ПО сервера баз данных:

+	операционная система Linux Ubuntu
+	СУБД MariaDB
+	PHP-FPM менеджер процессов.

CRM-система построена с использованием PHP-фреймворка Yii — это высокоэффективный основанный на компонентной структуре PHP-фреймворк для разработки масштабных веб-приложений. Он позволяющий максимально применить концепцию повторного использования кода и может существенно ускорить процесс веб-разработки.


### 1.4	Описание бизнес-процессов подлежащих автоматизации

####    Бизнес-процесс «Составление отчетов о работе команды»

Старший менеджер и глава отдела продаж, три раза в день, составляют отчеты о работе своих подчиненных, включающие количество и продолжительность телефонных разговоров используя отчеты в компоненте АТС — FreePBX. Также они составляют сводные отчеты в конце рабочего дня.

Пример отчета

| Сотрудник       | Иванов | Петров | Сидоров
| ------------- |------------------|------------------|------------------|
| Входящие звонки отвеченные | 0 | 1 | 0 |
| Входящие звонки неотвеченные | 0 | 2 | 0 |
| Исходящие звонки отвеченные | 32 | 14 | 22 |
| Исходящие звонки неотвеченные | 25 | 11 | 31 |
| Время входящих звонков | 0 | 10 минут 45 секунд | 0 |
| Время исходящих звонков | 2 часа 51 минута | 1 час 47 минут | 1 час 58 минут |
| Всего звонков | 32 | 15 | 22 |
| Общее время | 2 часа 51 минута | 1 час 58 минут | 1 час 58 минут |
*Таблица 5 — Пример отчета о работе сотрудников*

####	Бизнес-процесс «Контроль работы сотрудника»

Старший менеджер или глава отдела продаж, выборочно могут прослушать разговор любого подчиненного сотрудника, для контроля их работы, используя модуль прослушки АТС, путем набора специальной комбинации клавиш на телефонном аппарате.

####	Бизнес-процесс «Конвертация аудиофайлов»

АТС сохраняет запись разговора в формате потокового аудио WAV. Для экономии ресурсов хранилищ производится конвертация записи в формат MP3.

####	Бизнес-процесс «Прослушивание записи»

Старший менеджер или глава отдела продаж, выборочно могут прослушать разговор любого подчиненного сотрудника, после окончания разговора, для чего необходимо обратится к администратору АТС, с указанием даты и внутреннего и внешнего номера, на который был осуществлен звонок, и администратор при помощи интерфейса FreePBX найдет необходимую запись. Записи разговоров, на региональном сервере, хранятся три месяца. По истечении трех месяцев, записи разговоров автоматически перемещаются в архив.

####	Бизнес-процесс «Прослушивание записи из архива»

Старший менеджер или глава отдела продаж, выборочно могут прослушать разговор любого подчиненного сотрудника мз любого регионального подразделения, отправив запрос к системному администратору, с указанием даты и внутреннего и внешнего номера, на который был осуществлен звонок, после чего администратор осуществит поиск в архиве, и отдаст аудиофайл в случае успешного нахождения.

### 1.5	Разработка функциональной модели подсистемы «как есть»

Изобразим функиональную модель системы в виде IDEF0 для отображения бизнес-процессов и UML для отображения активности компонентов системы.

*Рисунок 2 — Функциональная модель автоматизируемого бизнес-процесса*
![Функциональная модель автоматизируемого бизнес-процесса IDEF0](diagrams/idef0_func.png)

Управление:
+   Скрипт продаж — шаблон общения с клиентом с заготовленными фразами и реакциями клиента на них;
+   Регламент общения с клиентом — свод правил, согласно которому должен происходить разговор.

Входы:
+   Карточка клиента — инфрмация о клиенте собранная системой за время работы с ним.

Механизмы:
+   АТС — система для звонка клиенту;
+   excel — составление отчетности о работе сотрудников;
+   storage — хранилище записей разговоров и информации о них;
+   СRM — хранение информации о клиенте.

Выходы:
+   Отчет о разговоре 
+   Запись разговора

*Рисунок 3 — Функциональная модель автоматизируемого бизнес-процесса*
![Функциональная модель автоматизируемого бизнес-процесса IDEF0](diagrams/idef_func.png)

Рассмотрим моделируемый процесс более подробно. При более детальном рассмотрении мы можем выделить в процессе четыре основных узла.

1. Разговор с клиентом — менеджер открывает карточку клиента с CRM и набирает номер клиента на телефонном аппарате, почле чего строит разговор с клиентом согласно нормативной документации, используя знания о клиенте сохраненные ранее в CRM.
2. Сохранить данные разговора — АТС сохраняет данные о разговоре и его запись.
3. Извлечь данные разговора — старший менеджер или глава отдела продаж обращаются к администратору АТС с просьбой предоставить им запись разговора, если разговор происходил менее 3-х месяцев назад или к системному администратору, если разговор происходил более 3-х месяцев назад.
4. Подготовить отчет — старший менеджер или глава отдела продаж формируют отчеты в excel, используя данные из хранилища, и отправляют их руководителям, которые формируют сводные отчеты.

Отобразим на схеме взаимодействие компонентов системы при сохранении записи.

*Рисунок 4 — Схема взаимодействия компонентов системы при сохранении записи*
![Функциональная модель автоматизируемого бизнес-процесса UML](diagrams/activity_simplified_as_is.png)

При поступлении входящего или исходящего звонка, Asterisk начинает запись служебной информации в базу данных и обновляет её в процессе разговора. Затем, по завершении разговора, происходит сохранение записи в хранилище, в формате wav, а в базе данных указывается название сохраненного файла. После этого скрипт конвертации находит необработанную запись в формате wav, конвертирует ее в формат mp3, и обновляет название файла в базе данных, на этом процесс завершается.

### 1.6	Схема документооборота

*Рисунок 5 — Схема документооборота*
![Схема документооборота](diagrams/workflow.png)

Менежер звонит клиенту для заключения сделок используя CRM содержащую в себе карточку клиента с необходимой для взаимодействия с клиентом информацией, такой как телефонный номер, имя, заинтересовавшие продукты, в формате отдельной страницы для каждого клиента. 
В процессе общения с клиентом, менеджер вносит в систему информацию о взаимодействии, и она становится доступна остальным сотрудникам.
Старший менеджер выборочно просматривает комментарии и прослушивает записи разговоров, после чего составляет отчеты отправляемые руководителю, каждый час и ежедневный итоговый.
АТС сохраняет записи телефонных разговоров в архив.
Если старший менеджер хочет прослушать запись не в режиме онлайн по телефону, а извлечь ее из архива — он обращается к одному из администраторов, если запись младше трех месяцев к администратору АТС, если запись старше трех месяцев, к системному администратору.

### 1.7	Выводы по главе

После анализа представленных бизнес-процессов, можно сделать вывод о том, что много времени сотрудников тратится на составление отчетности, информация для которых уже имеется в цифровом виде во внешних программах. 
При таких условиях оптимизация получения этих данных минуя сотрудников даст значительный прирост производительности.

Поэтому, разработка функциональной составляющей для ведения отчетности звонков является логичным шагом усовершенствования системы. Так как она может значительно увеличить скоростьи качество обслуживания клиентов, а значит улучшить качество сервиса в целом.

Функциональность создания отчетов необходима для автоматического расчета времени затраченного менеджером на основе статистики, которая ведется для каждого звонка, совершенного в колл-центре.

Также сотрудники отдела продаж довольно часто обращаются к администраторам с просьбами извлечения записей из архива, что негативно сказывается на их загруженности, поэтому необходимо предусмотреть в проектируемой системе автоматический доступ к любой записи разговора, совершенной подчиненным сотрудником.